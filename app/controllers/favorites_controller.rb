class FavoritesController < ApplicationController
  include Authorized
  include FavoriteHelpers

  def show
    @favorites = Tweet.includes(:mentions).where("id IN (#{favorites_query.select(:tweet_id).to_sql})").order(id: :desc)
    @favorites.each do |tweet|
      tweet.favorited_by_current_user = true
    end
  end
end
