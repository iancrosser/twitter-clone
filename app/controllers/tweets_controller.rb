class TweetsController < ApplicationController
  include Authorized

  def create
    flash.delete(:tweet_errors)
    params.require(:tweet)
    tweet = TweetService.create_tweet( params[:tweet].permit!.merge(author: current_user) )
    flash[:tweet_errors] = tweet.errors.full_messages unless tweet.persisted?
    redirect_to timeline_path
  end
end
