class FollowingsController < ApplicationController
  include Authorized
  include FollowingHelpers

  def show
    @followings = User.where("id IN (#{followings_query(current_user).select(:followee_id).to_sql})")
    @followings.each do |user|
      user.following_current_user = current_user_following_user?( user )
    end
  end
end
