class FollowersController < ApplicationController
  include Authorized
  include FollowingHelpers

  def show
    @followers = User.where("id IN (#{followers_query(current_user).select(:follower_id).to_sql})")
    @followers.each do |user|
      user.following_current_user = current_user_following_user?( user )
    end
  end
end
