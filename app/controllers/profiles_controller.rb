# the user profiles controller
class ProfilesController < ApplicationController
  include Authorized

  def show
  end

  def edit
  end

  def update
    params.require(:user)
    @user = current_user
    if @user.update( params[:user].permit! )
      redirect_to profile_path
    else
      render :edit
    end
  end
end
