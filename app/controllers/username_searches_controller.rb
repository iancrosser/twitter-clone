class UsernameSearchesController < ApplicationController
  def create
    @users = User.username_search( params[:query] )
  end
end
