# the users controller
class UsersController < ApplicationController
  include Authorized
  include FollowingHelpers

  skip_before_action :require_user, only: [:new, :create]

  def new
    redirect_to timeline_path if signed_in?

    @recent_tweets = Tweet.includes(:mentions).order(id: :desc).limit(10)
  end

  def create
    params.require(:user)

    @user = User.create( params[:user].permit! )
    if @user.persisted?
      session[:user_id] = @user.id
      redirect_to timeline_path
    else
      render :new
    end
  end

  def show
    @user = User.find_by( username: params[:id] )
    @following = current_user_following_user?( @user )

    @recent_tweets = Tweet.includes(:mentions).where(author_id: @user.id).order(id: :desc).limit(10)
  end
end
