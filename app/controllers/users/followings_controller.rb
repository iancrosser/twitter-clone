module Users
  class FollowingsController < ApplicationController
    include Authorized
    include FollowingHelpers

    def create
      Following.create( following_query_params(followed_user) )
      redirect_to user_path( params[:user_id] )
    end

    def destroy
      following_query( followed_user ).delete_all
      redirect_to user_path( params[:user_id] )
    end

    private

    def followed_user
      User.find_by(username: params[:user_id])
    end
  end
end # module Users
