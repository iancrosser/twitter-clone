# for making sure that users are authorized
module Authorized
  extend ActiveSupport::Concern

  included do
    include CurrentUser
    before_action :require_user
  end

  def require_user
    redirect_to root_path unless signed_in?
  end
end
