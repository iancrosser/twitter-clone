# helpers for making figuring out favorites easier
module FavoriteHelpers
  extend ActiveSupport::Concern

  included do
    include CurrentUser
  end

  def current_user_favorited_tweet?( tweet )
    favorite_query( tweet ).count > 0
  end

  private

  def favorite_query_params( tweet )
    {
      user_id: current_user.id,
      tweet_id: tweet.id
    }
  end

  def favorite_query( tweet )
    Favorite.where( favorite_query_params(tweet) )
  end

  def favorites_query
    Favorite.where( user_id: current_user.id )
  end
end
