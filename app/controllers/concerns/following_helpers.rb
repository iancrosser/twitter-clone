# helpers for making figuring out followings easier
module FollowingHelpers
  extend ActiveSupport::Concern

  included do
    include CurrentUser
  end

  def current_user_following_user?( user )
    following_query( user ).count > 0
  end

  private

  def following_query_params( user )
    {
      follower_id: current_user.id,
      followee_id: user.id
    }
  end

  def following_query( user )
    Following.where( following_query_params(user) )
  end

  def followings_query( user )
    Following.where( follower_id: user.id )
  end

  def followers_query( user )
    Following.where( followee_id: user.id )
  end
end
