# helper for making current_user available in controller and views
module CurrentUser
  extend ActiveSupport::Concern

  included do
    helper_method :current_user
    helper_method :signed_in?
  end

  def current_user
    @current_user ||= User.find(session[:user_id].to_i)
  rescue ActiveRecord::RecordNotFound
    nil
  end

  def signed_in?
    current_user.present?
  end
end
