# the timeline controller
class TimelinesController < ApplicationController
  include Authorized
  include FollowingHelpers
  include FavoriteHelpers

  def show
    @timeline_tweets = Tweet.includes(:mentions).where("author_id = ? OR author_id IN (#{followings_query( current_user ).select(:followee_id).to_sql})", current_user.id).order(id: :desc).limit(10)
    @timeline_tweets.each do |tweet|
      tweet.favorited_by_current_user = current_user_favorited_tweet?( tweet )
    end
  end
end
