# for creating sessions
class SessionsController < ApplicationController
  def create
    unless current_user.present?
      user = User.find_by_email_or_username( params[:email_or_username] )
      if user.present? && user.authenticate( params[:password] ).present?
        session[:user_id] = user.id
      end
    end
    redirect_to root_path
  end

  def destroy
    session.delete(:user_id)
    redirect_to root_path
  end
end
