module Tweets
  class FavoritesController < ApplicationController
    include Authorized
    include FavoriteHelpers

    def create
      Favorite.create( favorite_query_params(tweet) )
      redirect_to request.referer
    end

    def destroy
      Favorite.where( favorite_query_params(tweet) ).delete_all
      redirect_to request.referer
    end

    private

    def tweet
      Tweet.find(params[:tweet_id])
    end
  end
end # module Tweets
