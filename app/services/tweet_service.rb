# service that knows about everything tweet related
class TweetService
  class << self # class methods
    def create_tweet( tweet_attributes )
      tweet = Tweet.create( tweet_attributes )
      populate_mentions( tweet )
      tweet
    end

    private

    def populate_mentions( tweet)
      if tweet.persisted?
        tweet.content.split(Mention.delimiter).each do |potential_username|
          mentioned_user = User.where(username: potential_username.match(/^([\w\-]+)/).to_s).first
          unless mentioned_user.nil?
            Mention.create(
              tweet: tweet,
              user: mentioned_user,
              replace_string: "#{Mention.delimiter}#{mentioned_user.username}"
            )
          end
        end
      end
    end
  end # class methods
end
