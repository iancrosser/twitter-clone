# model representing a user following another user
class Following < ApplicationRecord
  belongs_to :follower, class_name: User
  belongs_to :followee, class_name: User

  validates :followee_id, presence: true
  validates :follower_id, presence: true, uniqueness: { scope: [:followee_id] }
end
