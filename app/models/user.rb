# the user model
class User < ApplicationRecord
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  mappings do
    indexes :username, type: 'keyword'
  end

  has_secure_password
  attr_accessor :following_current_user

  validates :email, presence: true, format: { with: /.*@.*/ }, uniqueness: true
  validates :username, presence: true, format: { with: /\A[A-Za-z][A-Za-z_0-9]*\z/ }, uniqueness: true

  class << self # class methods
    def find_by_email_or_username( email_or_username )
      return nil if email_or_username.nil?

      if email_or_username.include?('@')
        User.find_by(email: email_or_username)
      else
        User.find_by(username: email_or_username)
      end
    end

    def username_search( username )
      where(id: search(
        query: {
          prefix: {
            username: {
              value: username
            }
          }
        }
      ).records.ids)
    end
  end # class methods
end # User
