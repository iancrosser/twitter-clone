# the tweet model
class Tweet < ApplicationRecord
  belongs_to :author, class_name: User
  has_many :mentions

  attr_accessor :favorited_by_current_user

  validates :content, presence: true
  validates :author_id, presence: true
end # Tweet
