# model to represent a mention of a user from a tweet
class Mention < ApplicationRecord
  belongs_to :tweet
  belongs_to :user

  validates :tweet_id, presence: true
  validates :user_id, presence: true, uniqueness: {scope: [:tweet_id]}
  validates :replace_string, presence: true

  class << self # class methods
    def delimiter
      '@'
    end
  end # class methods
end # Mention
