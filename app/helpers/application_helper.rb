module ApplicationHelper
  def mention_link_content( tweet )
    return tweet.content if tweet.mentions.empty?

    tweet.mentions.reduce(tweet.content) do |linked_content, mention|
      linked_content.gsub(
        mention.replace_string,
        link_to("#{mention.replace_string}", user_path(mention.user.username))
      )
    end.html_safe
  end
end
