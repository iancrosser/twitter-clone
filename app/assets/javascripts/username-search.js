var usernameQueryTimeout = undefined;
var $usernameQueryResults = undefined;

$(document).on('turbolinks:load', function() {
});

$(document).on('keyup', '#username-query-results input', function(evt) {
  var $input = $(evt.currentTarget);
  var $form = $input.parents('form');
  if( $usernameQueryResults != undefined ) {
    try {
      $usernameQueryResults.popover('destroy');
    } catch(err) {
      // don't care really
    }
  }
  if( usernameQueryTimeout != undefined ) {
    clearTimeout(usernameQueryTimeout);
  }
  usernameQueryTimeout = setTimeout(function(){
    if( $input.val() !== '' ) {
      $form.submit();
    }
  }, 400);
});

