# who needs factory girl when you can roll your own ¯\_(ツ)_/¯
module ObjectCreationMethods
  # Mention related methods
  def mention_default_attributes
    built_user = build_user
    {
      tweet: build_tweet,
      user: built_user,
      replace_string: "@#{built_user.username}"
    }
  end

  def build_mention( overrides = {} )
    mention_attributes = mention_default_attributes.merge(overrides)
    mention_attributes[:tweet].save!
    mention_attributes[:user].save!
    Mention.new( mention_attributes )
  end

  def create_mention( overrides = {} )
    mention = build_mention( overrides )
    mention.save!
    mention
  end

  # Favorite related methods
  def favorite_default_attributes
    {
      tweet: build_tweet,
      user: build_user
    }
  end

  def build_favorite( overrides = {} )
    favorite_attributes = favorite_default_attributes.merge(overrides)
    favorite_attributes[:tweet].save!
    favorite_attributes[:user].save!
    Favorite.new( favorite_attributes )
  end

  def create_favorite( overrides = {} )
    favorite = build_favorite( overrides )
    favorite.save!
    favorite
  end

  # Following related mtehods
  def following_default_attributes
    {
      follower: build_user,
      followee: build_user
    }
  end

  def build_following( overrides = {} )
    following_attributes = following_default_attributes.merge(overrides)
    following_attributes[:follower].save!
    following_attributes[:followee].save!
    Following.new( following_attributes )
  end

  def create_following( overrides = {} )
    following = build_following( overrides )
    following.save!
    following
  end

  # Tweet related methods
  def tweet_default_attributes
    {
      content: 'Making tweeting great again',
      author: build_user
    }
  end

  def build_tweet( overrides = {} )
    tweet_attributes = tweet_default_attributes.merge(overrides)
    tweet_attributes[:author].save!
    Tweet.new( tweet_attributes )
  end

  def create_tweet( overrides = {} )
    tweet = build_tweet( overrides )
    tweet.save!
    tweet
  end

  # User related methods
  def user_default_attributes
      {
        email: Faker::Internet.unique.email,
        username: Faker::Internet.user_name(
          Faker::Name.unique.name,
          %w(_)
        ),
        password: 'da password',
        password_confirmation: 'da password'
      }
  end

  def build_user( overrides = {} )
    User.new( user_default_attributes.merge(overrides) )
  end

  def create_user( overrides = {} )
    user = build_user(overrides)
    user.save!
    user
  end
end
