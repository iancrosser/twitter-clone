module AuthenticationHelperMethods

  class SignOutError < Exception;end

  def sign_in( user )
    request.session[:user_id] = user.id
  end

  def sign_out( user = nil )
    if user.nil?
      request.session[:user_id] = nil
    else
      if request.session[:user_id] == user.id
        request.session.delete( :user_id )
      else
        raise SignOutError.new("User ##{user.id} is not signed in. session[:user_id] = #{request.session[:user_id]}")
      end
    end
  end
end
