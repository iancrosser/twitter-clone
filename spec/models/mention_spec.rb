require 'rails_helper'

describe Mention do
  describe 'class methods' do
    describe '::delimiter' do
      let(:subject) { described_class.delimiter }

      it 'should be the at sign' do
        expect( subject ).
            to eq( '@' )
      end
    end # describe '::delimiter'
  end # describe 'class methods'
  describe 'associastions' do
    it 'should belong to a tweet' do
      association = described_class.reflect_on_association(:tweet)

      expect(association.macro).
          to eq(:belongs_to)
    end

    it 'should belong to a user' do
      association = described_class.reflect_on_association(:user)

      expect(association.macro).
          to eq(:belongs_to)
    end
  end # describe 'associastions'

  describe 'validations' do
    let(:valid_attributes) { mention_default_attributes }
    let(:subject) { described_class.new(valid_attributes).valid? }

    it 'should require tweet_id' do
      valid_attributes.delete( :tweet )
      expect( subject ).
          to eq( false )
    end

    it 'should require user_id' do
      valid_attributes.delete( :user )
      expect( subject ).
          to eq( false )
    end

    it 'should require replace_string' do
      valid_attributes.delete( :replace_string )
      expect( subject ).
          to eq( false )
    end
  end # describe 'validations'
end # describe Mention
