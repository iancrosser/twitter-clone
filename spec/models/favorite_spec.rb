require 'rails_helper'

describe Favorite do
  describe 'associastions' do
    it 'should belong to a tweet' do
      association = described_class.reflect_on_association(:tweet)

      expect(association.macro).
          to eq(:belongs_to)
    end

    it 'should belong to a user' do
      association = described_class.reflect_on_association(:user)

      expect(association.macro).
          to eq(:belongs_to)
    end
  end # describe 'associastions'

  describe 'validations' do
    let(:valid_attributes) { favorite_default_attributes }
    let(:subject) { described_class.new(valid_attributes).valid? }

    it 'should require a tweet_id' do
      valid_attributes.delete( :tweet_id )
      expect( subject ).
          to eq( false )
    end

    it 'should require a user_id' do
      valid_attributes.delete( :user_id )
      expect( subject ).
          to eq( false )
    end

    it 'should check for uniquness of the tweet_id to the user_id' do
      described_class.create(valid_attributes)
      expect( subject ).
          to eq( false )
    end
  end # describe 'validations'
end # describe Favorite
