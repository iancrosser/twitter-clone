require 'rails_helper'

describe User do
  context 'class methods' do
    describe '::find_by_email_or_username' do
      let(:subject) { described_class.find_by_email_or_username(email_or_username) }

      context 'with nil' do
        let(:email_or_username) { nil }

        it 'should return nil' do
          expect( subject ).
              to eq( nil )
        end
      end

      context 'with email' do
        let(:email_or_username) { user_default_attributes[:email] }

        context 'that does not exist' do
          it 'should return nil' do
            expect( subject ).
                to eq( nil )
          end
        end # context 'that does not exist'

        context 'that does exist' do
          let(:user) { create_user( email: email_or_username ) }

          before do
            user # create the user
          end

          it 'should return the user' do
            expect( subject ).
                to eq( user )
          end
        end # context 'that does exist'
      end # context 'email'

      context 'username' do
        let(:email_or_username) { user_default_attributes[:username] }

        context 'that does not exist' do
        end # context 'that does not exist'

        context 'that does exist' do
          let(:user) { create_user( username: email_or_username ) }

          before do
            user # create the user
          end

          it 'should return the user' do
            expect( subject ).
                to eq( user )
          end
        end # context 'that does exist'
      end # context 'username
    end # describe '::find_by_email_or_username'

    describe '::username_search' do
      it 'should prefix search the username' do
        username_search = Faker::Internet.name

        expect( described_class ).
            to receive(:search).
            with(
              query: {
                prefix: {
                  username: {
                    value: username_search
                  }
                }
              }
            ).
            and_return double('Elasticsearch::Model::Response::Response', records: double('Elasticsearch::Model::Response::Records', ids: []))

        described_class.username_search( username_search )
      end
    end # describe '::username_search'
  end # context 'class methods'

  context 'validations' do
    let(:valid_attributes) { user_default_attributes }

    context 'when new' do
      let(:subject) { described_class.new(valid_attributes).valid? }

      it 'should require an email' do
        valid_attributes.delete(:email)
        expect( subject ).
            not_to eq( true )
      end

      it 'should require a valid email' do
        valid_attributes.merge!(email: valid_attributes[:username])
        expect( subject ).
            not_to eq( true )
      end

      it 'should require a unique email' do
        user = create_user
        valid_attributes.merge!(email: user.email)
        expect( subject ).
            not_to eq( true )
      end

      it 'should require a username' do
        valid_attributes.delete(:username)
        expect( subject ).
            not_to eq( true )
      end

      it 'should require a valid username' do
        valid_attributes.merge!(username: valid_attributes[:email])
        expect( subject ).
            not_to eq( true )
      end

      it 'should require a unique username' do
        user = create_user
        valid_attributes.merge!(username: user.username)
        expect( subject ).
            not_to eq( true )
      end

      it 'should require a password' do
        valid_attributes.delete(:password)
        expect( subject ).
            not_to eq( true )
      end

      it 'should require a matching password_confirmation' do
        valid_attributes.merge!(password_confirmation: "#{valid_attributes['password']} does not match")
        expect( subject ).
            not_to eq( true )
      end
    end # context 'when new'

    context 'when persisted' do
      let(:model) { described_class.create!(valid_attributes) }
      let(:subject) { model.update(valid_attributes) }

      before do
        model # create the model
      end

      it 'should require an email' do
        valid_attributes.merge!(email: nil)
        expect( subject ).
            not_to eq( true )
      end

      it 'should require a username' do
        valid_attributes.merge!(username: nil)
        expect( subject ).
            not_to eq( true )
      end

      it 'should require a password fields are missing' do
        valid_attributes.delete(:password)
        valid_attributes.delete(:password_confirmation)
        expect( subject ).
            to eq( true )
      end
    end # context 'when persisted'
  end # context 'validations'

  context 'attr_accessors' do
    let(:subject) { described_class.new }
    it 'should include following_current_user' do
      expect( subject.attributes.keys ).
          not_to include( 'following_current_user' )
      expect( subject.respond_to?(:following_current_user) ).
          to eq( true )
      expect( subject.respond_to?(:following_current_user=) ).
          to eq( true )
    end
  end # context attr_accessors'
end # describe User
