require 'rails_helper'

describe Tweet do
  describe 'associastions' do
    it 'should belong to an author' do
      association = described_class.reflect_on_association(:author)

      expect(association.macro).
          to eq(:belongs_to)
    end

    it 'should have many mentions' do
      association = described_class.reflect_on_association(:mentions)

      expect(association.macro).
          to eq(:has_many)
    end
  end # describe 'associastions'

  describe 'validations' do
    let(:valid_attributes) { build_tweet.attributes }
    let(:subject) { described_class.new(valid_attributes).valid? }

    it 'should be valid with valid attributes' do
      expect( subject ).
          to eq( true )
    end

    it 'should require content' do
      valid_attributes.delete( 'content')
      expect( subject ).
          to eq( false )
    end

    it 'should require an author_id' do
      valid_attributes.delete( 'author_id')
      expect( subject ).
          to eq( false )
    end
  end # describe 'validations'

  context 'attr_accessors' do
    let(:subject) { described_class.new }
    it 'should include favorited_by_current_user' do
      expect( subject.attributes.keys ).
          not_to include( 'favorited_by_current_user' )
      expect( subject.respond_to?(:favorited_by_current_user) ).
          to eq( true )
      expect( subject.respond_to?(:favorited_by_current_user=) ).
          to eq( true )
    end
  end # context attr_accessors'
end # describe Tweet
