require 'rails_helper'

describe Following do
  describe 'associastions' do
    it 'should belong to a follower' do
      association = described_class.reflect_on_association(:follower)

      expect(association.macro).
          to eq(:belongs_to)
    end

    it 'should belong to a followee' do
      association = described_class.reflect_on_association(:followee)

      expect(association.macro).
          to eq(:belongs_to)
    end
  end # describe 'associastions'

  describe 'validations' do
    let(:valid_attributes) { following_default_attributes }
    let(:subject) { described_class.new(valid_attributes).valid? }

    it 'should require a follower_id' do
      valid_attributes.delete( :follower_id )
      expect( subject ).
          to eq( false )
    end

    it 'should require a followee_id' do
      valid_attributes.delete( :followee_id )
      expect( subject ).
          to eq( false )
    end

    it 'should check for uniquness of the follower_id to the followee_id' do
      described_class.create(valid_attributes)
      expect( subject ).
          to eq( false )
    end
  end # describe 'validations'
end # describe Following
