require 'rails_helper'

describe TweetService do
  describe 'class methods' do
    describe 'create_tweet' do
      let(:tweet_attributes) { tweet_default_attributes }
      let(:subject) { described_class.create_tweet( tweet_attributes ) }

      before do
        tweet_attributes[:author].save!
      end

      it 'should create tweets' do
        expect{ subject }.
            to change{ Tweet.count }.
            by( 1 )
      end

      context 'with mentioned users' do
        let(:mentioned_user1) { create_user }
        let(:mentioned_user2) { create_user }

        before do
          tweet_attributes.merge!( content: "hey #{Mention.delimiter}#{mentioned_user1.username} do you like #{Mention.delimiter}#{mentioned_user2.username}?" )
        end

        it 'should create mentions' do
          expect{ subject }.
              to change{ Mention.count }.
              by( 2 )
        end
      end # context 'with mentioned users'
    end # describe 'create tweet'
  end # describe 'class methods'
end # describe TweetService
