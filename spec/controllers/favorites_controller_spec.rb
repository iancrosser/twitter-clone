require 'rails_helper'

describe FavoritesController do
  context 'GET show' do
    let(:subject) { get :show }

    context 'in general' do
      it_should_behave_like 'an authorized method', { via: :get, method: :show }
    end # context 'in general'

    context 'with a signed in user' do
      let(:user) { create_user }
      let(:tweet) { create_tweet }

      before do
        sign_in user

        create_favorite(
          user: user,
          tweet: tweet
        )
      end

      it 'should assign favorites' do
        subject

        expect( assigns[:favorites] ).
            to eq( [tweet] )
      end
    end # context 'with a signed in user'

  end # context 'GET show'
end # describe FavoritesController
