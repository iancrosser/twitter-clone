require 'rails_helper'

describe TweetsController do
  describe 'POST create' do
    context 'in general' do
      it_should_behave_like 'an authorized method', { via: :post, method: :create, params: {tweet: {content: 'the way tweeting should be'}} }
    end

    context 'with a signed in user' do
      let(:user) { create_user }
      let(:post_params) { {} }
      let(:subject) { post :create, params: post_params }

      before do
        sign_in user
      end

      context 'with valid attributes' do
        before do
          post_params.merge!( tweet: { content: 'does this tweet make me cool?' } )
        end

        it 'should create a tweet authored by the current user' do
          expect{ subject }.
              to change{ Tweet.where(author_id: user.id).count }.
              by( 1 )
        end

        it 'should redirect to the timeline path' do
          expect( subject ).
              to redirect_to timeline_path
        end

        it 'should not set a flash error' do
          subject
          expect( flash[:tweet_errors] ).
              to eq( nil )
        end
      end # context 'with valid attributes'

      context 'with invalid attributes' do
        before do
          post_params.merge!( tweet: {content: ''} )
        end

        it 'should not create a tweet authored by the current user' do
          expect{ subject }.
              not_to change{ Tweet.where(author_id: user.id).count }
        end

        it 'should redirect to the timeline path' do
          expect( subject ).
              to redirect_to timeline_path
        end

        it 'should set a flash error' do
          subject
          expect( flash[:tweet_errors] ).
              not_to eq( nil )
        end
      end # context 'with invalid attributes'
    end
  end # describe 'POST create'
end
