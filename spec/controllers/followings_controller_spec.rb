require 'rails_helper'

describe FollowingsController do
  context 'GET show' do
    let(:subject) { get :show }

    context 'in general' do
      it_should_behave_like 'an authorized method', { via: :get, method: :show }
    end # context 'in general'

    context 'with a signed in user' do
      let(:user) { create_user }
      let(:following_user) { create_user }

      before do
        sign_in user

        create_following(
          followee: following_user,
          follower: user
        )
      end

      it 'should assign followings' do
        subject

        expect( assigns[:followings] ).
            to eq( [following_user] )
      end
    end # context 'with a signed in user'

  end # context 'GET show'
end # describe FollowingsController
