require 'rails_helper'

describe FollowersController do
  context 'GET show' do
    let(:subject) { get :show }

    context 'in general' do
      it_should_behave_like 'an authorized method', { via: :get, method: :show }
    end # context 'in general'

    context 'with a signed in user' do
      let(:user) { create_user }
      let(:followed_user) { create_user }

      before do
        sign_in user

        create_following(
          followee: user,
          follower: followed_user
        )
      end

      it 'should assign followers' do
        subject

        expect( assigns[:followers] ).
            to eq( [followed_user] )
      end
    end # context 'with a signed in user'

  end # context 'GET show'
end # describe FollowersController
