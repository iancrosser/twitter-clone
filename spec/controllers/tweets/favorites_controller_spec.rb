require 'rails_helper'

describe Tweets::FavoritesController do
  let(:user) { create_user }
  let(:tweet) { create_tweet }
  let(:referrer) { 'http://referrer.example.com' }

  before do
    request.headers.merge!( 'HTTP_REFERER' => referrer )
  end

  context 'POST create' do
    context 'in general' do
      before do
        allow( Tweet ).
            to receive( :find ).
            and_return create_tweet
      end

      it_should_behave_like 'an authorized method', { via: :post, method: :create, params: {tweet_id: -1} }
    end # context 'in general'

    context 'with a signed in user' do
      let(:subject) { post :create, params: { tweet_id: tweet.id } }

      before do
        sign_in user
      end

      context 'and the favorite is new' do
        it 'should create a favorite' do
          expect{ subject }.
              to change{ Favorite.where(user_id: user.id).count }.
              by(1)
        end

        it 'should redirect to the referrer' do
          expect( subject ).
              to redirect_to( referrer )
        end
      end # context 'and the favorite is new'

      context 'and the favorite already exists' do
        before do
          create_favorite(
            user: user,
            tweet: tweet
          )
        end

        it 'should redirect to the followee page' do
          expect( subject ).
              to redirect_to( referrer )
        end
      end # context 'and the favorite already exists'
    end # context 'with a signed in user'
  end

  context 'DELETE destroy' do
    context 'in general' do
      before do
        allow( Tweet ).
            to receive( :find ).
            and_return create_tweet
      end

      it_should_behave_like 'an authorized method', { via: :delete, method: :destroy, params: {tweet_id: -1} }
    end # context 'in general'

    context 'with a signed in user' do
      let(:user) { create_user }
      let(:subject) { delete :destroy, params: { tweet_id: tweet.id }, headers: { 'HTTP_REFERER' => referrer } }

      before do
        sign_in user
      end

      context 'and there is no existing favorite' do
        it 'should redirect to the followee page' do
          expect( subject ).
              to redirect_to( referrer )
        end
      end # context 'and there is no existing favorite'

      context 'and the favorite already exists' do
        before do
          create_favorite(
            user: user,
            tweet: tweet
          )
        end

        it 'should destroy the favorite' do
          expect{ subject }.
              to change{ Favorite.where(user_id: user.id).count }.
              by(-1)
        end

        it 'should redirect to the followee page' do
          expect( subject ).
              to redirect_to( referrer )
        end
      end # context 'and the favorite already exists'
    end # context 'with a signed in user'
  end
end # describe Users::FollowingsController
