require 'rails_helper'

describe UsernameSearchesController do
  context 'POST create' do
    let(:subject) { post :create, params: { query: 'username_query' }, format: :js }

    it 'should find users by username' do
      expect( User ).
          to receive( :username_search ).
          with( 'username_query' )

      subject
    end

    it 'should assign users' do
      subject

      expect( assigns[:users] ).
          not_to eq( nil )
    end
  end # context 'POST create'
end # describe UsernameSearchesController
