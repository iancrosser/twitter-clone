require 'rails_helper'

describe Users::FollowingsController do
  let(:user) { create_user }
  let(:followed_user) { create_user }

  context 'POST create' do
    context 'in general' do
      before do
        create_user( username: 'username' )
      end

      it_should_behave_like 'an authorized method', { via: :post, method: :create, params: {user_id: 'username'} }
    end # context 'in general'

    context 'with a signed in user' do
      let(:subject) { post :create, params: { user_id: followed_user.username } }

      before do
        sign_in user
      end

      context 'and the following is new' do
        it 'should create a following' do
          expect{ subject }.
              to change{ Following.where(follower_id: user.id).count }.
              by(1)
        end

        it 'should redirect to the followee page' do
          expect( subject ).
              to redirect_to( user_path(followed_user.username) )
        end
      end # context 'and the following is new'

      context 'and the following already exists' do
        before do
          create_following(
            follower: user,
            followee: followed_user
          )
        end

        it 'should redirect to the followee page' do
          expect( subject ).
              to redirect_to( user_path(followed_user.username) )
        end
      end # context 'and the following already exists'
    end # context 'with a signed in user'
  end

  context 'DELETE destroy' do
    context 'in general' do
      before do
        create_user( username: 'username' )
      end

      it_should_behave_like 'an authorized method', { via: :delete, method: :destroy, params: {user_id: 'username'} }
    end # context 'in general'

    context 'with a signed in user' do
      let(:user) { create_user }
      let(:subject) { delete :destroy, params: { user_id: followed_user.username } }

      before do
        sign_in user
      end

      context 'and there is no existing following' do
        it 'should redirect to the followee page' do
          expect( subject ).
              to redirect_to( user_path(followed_user.username) )
        end
      end # context 'and there is no existing following'

      context 'and the following already exists' do
        before do
          create_following(
            follower: user,
            followee: followed_user
          )
        end

        it 'should destroy the following' do
          expect{ subject }.
              to change{ Following.where(follower_id: user.id).count }.
              by(-1)
        end

        it 'should redirect to the followee page' do
          expect( subject ).
              to redirect_to( user_path(followed_user.username) )
        end
      end # context 'and the following already exists'
    end # context 'with a signed in user'
  end
end # describe Users::FollowingsController
