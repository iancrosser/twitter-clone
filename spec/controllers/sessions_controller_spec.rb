require 'rails_helper'

describe SessionsController do
  let(:password) { 'I like turtles' }
  let(:user) {
    create_user(
      password: password,
      password_confirmation: password
    )
  }

  describe 'POST create' do
    let(:post_params) { {} }
    let(:subject) { post :create, params: post_params }

    context 'with a signed in user' do
      before do
        sign_in user
      end

      it 'should redirect to the root path' do
        expect( subject ).
            to redirect_to( root_path )
      end
    end # context 'with a signed in user'

    context 'with no signed in user' do
      before do
        sign_out
      end

      context 'with valid email' do
        before do
          post_params.merge!( email_or_username: user.email )
        end

        context 'with correct password' do
          before do
            post_params.merge!( password: password )
          end

          it 'should set the current user in the session' do
            expect{ subject }.
                to change{ session[:user_id] }.
                from( nil ).
                to( user.id )
          end
        end # context 'with correct password'

        context 'with incorrect password' do
          before do
            post_params.merge!( password: "wrong #{password}" )
          end

          it 'should redirect to the root path' do
            expect( subject ).
                to redirect_to( root_path )
          end
        end # context 'with in correct password'
      end # context 'with valid email'

      context 'with invalid email' do
        before do
          post_params.merge!( email_or_username: user_default_attributes[:email] )
        end

        it 'should redirect to the root path' do
          expect( subject ).
              to redirect_to( root_path )
        end
      end # context 'with invalid email'

      context 'with valid username' do
        before do
          post_params.merge!( email_or_username: user.username )
        end

        context 'with correct password' do
          before do
            post_params.merge!( password: password )
          end

          it 'should set the current user in the session' do
            expect{ subject }.
                to change{ session[:user_id] }.
                from( nil ).
                to( user.id )
          end
        end # context 'with correct password'

        context 'with incorrect password' do
          before do
            post_params.merge!( password: "wrong #{password}" )
          end

          it 'should redirect to the root path' do
            expect( subject ).
                to redirect_to( root_path )
          end
        end # context 'with in correct password'
      end # context 'with valid username'

      context 'with invalid username' do
        before do
          post_params.merge!( email_or_username: user_default_attributes[:username] )
        end

        it 'should redirect to the root path' do
          expect( subject ).
              to redirect_to( root_path )
        end
      end # context 'with in valid username'
    end # context 'with no signed in user'
  end # describe 'POST create'

  describe 'GET destroy' do
    let(:subject) { get :destroy }

    context 'with no signed in user' do
      it 'should redirect to the root path' do
        expect( subject ).
            to redirect_to( root_path )
      end
    end # context 'with no signed in user'

    context 'with a signed in user' do
      before do
        sign_in user
      end

      it 'should redirect to the root path' do
        expect( subject ).
            to redirect_to( root_path )
      end

      it 'should remove the session' do
        expect{ subject }.
            to change{ session[:user_id] }.
            from( user.id ).
            to( nil )
      end
    end # context 'with a signed in user'
  end
end
