require 'rails_helper'

describe TimelinesController do
  describe 'GET show' do
    context 'in general' do
      it_should_behave_like 'an authorized method', { via: :get, method: :show }
    end

    context 'with a signed in user' do
      let(:user) { create_user }
      let(:following_user) { create_user }
      let(:followed_user) { create_user }
      let(:subject) { get :show }
      let(:user_tweet) {
        create_tweet(
          content: LiterateRandomizer.sentence,
          author: user
        )
      }
      let(:followed_user_tweet) {
        create_tweet(
          content: LiterateRandomizer.sentence,
          author: followed_user
        )
      }
      let(:following_user_tweet) {
        create_tweet(
          content: LiterateRandomizer.sentence,
          author: following_user
        )
      }

      before do
        sign_in user

        # create tweets
        user_tweet
        sleep 0.25 # to get seperation in the timings
        followed_user_tweet
        sleep 0.25 # to get seperation in the timings
        following_user_tweet

        # setup the followings
        create_following(
          followee: followed_user,
          follower: user
        )

        create_following(
          followee: user,
          follower: following_user
        )
      end

      it 'should find the users timeline' do
        subject

        expect( assigns[:timeline_tweets].map(&:id) ).
            to eq( [followed_user_tweet.id, user_tweet.id] )
      end
    end # context 'with a signed in user'
  end # describe 'POST create'
end
