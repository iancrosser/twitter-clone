require 'rails_helper'

describe ProfilesController do
  describe 'PUT update' do
    context 'in general' do
      it_should_behave_like 'an authorized method', { via: :put, method: :update, params: {user: {username: 'somethingnewandunique'}} }
    end # context 'in general'

    context 'with a signed in user' do
      let(:user) { create_user }
      let(:post_params) { {} }
      let(:subject) { put :update, params: post_params }

      before do
        sign_in user
      end

      context 'with valid attributes' do
        before do
          post_params.merge!( user: {username: user_default_attributes[:username]} )
        end

        it 'should update the current user' do
          expect{ subject }.
              to change{ User.find( user.id ).username }
        end

        it 'should redirect to the profile page' do
          expect( subject ).
              to redirect_to profile_path
        end
      end # context 'with valid attributes'

      context 'with invalid attributes' do
        before do
          post_params.merge!( user: {password: 'what I should be', password_confirmation: 'what I am not'} )
        end

        it 'should render edit' do
          expect( subject ).
              to render_template :edit
        end

        it 'should have error messages' do
          subject

          expect( assigns[:user].try(:errors) ).
              to_not eq( nil )
        end
      end # context 'with invalid attributes'
    end # context 'with a signed in user'
  end # describe 'PUT update'
end # describe ProfilesController
