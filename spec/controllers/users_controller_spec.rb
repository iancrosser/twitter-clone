require 'rails_helper'

describe UsersController do
  describe 'GET new' do
    let(:subject) { get :new }

    context 'with a signed in user' do
      before do
        sign_in create_user
      end

      it 'should redirect correctly' do
        expect( subject ).
            to redirect_to( timeline_path )
      end
    end # context 'with a signed in user'

    context 'without a signed in user' do
      before do
        sign_out
      end

      it 'should render new' do
        expect( subject ).
            to render_template :new
      end

      it 'should find the most recent tweets' do
        subject

        expect( assigns[:recent_tweets] ).
            not_to eq( nil )
      end
    end # context 'without a signed in user'
  end # describe 'GET new'

  describe 'POST create' do
    let(:post_params) { {} }
    let(:subject) { post :create, params: post_params }

    context 'with valid attributes' do
      before do
        post_params.merge!( user: user_default_attributes )
      end

      it 'should create a user' do
        expect{ subject }.
            to change{ User.count }.
            by( 1 )
      end

      it 'should redirect to the timeline' do
        expect( subject ).
            to redirect_to timeline_path
      end

      it 'should log the user in' do
        expect{ subject }.
            to change{ session[:user_id] }.
            from( nil )
      end
    end # context 'with valid attributes'

    context 'with invalid attributes' do
      before do
        post_params.merge!( user: user_default_attributes )
        post_params[:user].delete( :password )
      end

      it 'should render new' do
        expect( subject ).
            to render_template :new
      end

      it 'should have error messages' do
        subject

        expect( assigns[:user].try(:errors) ).
            to_not eq( nil )
      end
    end # context 'with invalid attributes'
  end # describe 'POST create'

  describe 'GET show' do
    context 'in general' do
      before do
        create_user( username: 'username' )
      end

      it_should_behave_like 'an authorized method', { via: :get, method: :show, params: {id: 'username'} }
    end # context 'in general'

    context 'with a signed in user' do
      let(:user) { create_user }
      let(:subject) { get :show, params: { id: user.username } }

      before do
        sign_in user
      end

      it 'should assign the user' do
        subject

        expect( assigns[:user] ).
            to eq( user )
      end

      it 'should assign the following' do
        subject

        expect( assigns[:following] ).
            not_to eq( nil )
      end

      it 'should assign recent_tweets' do
        subject

        expect( assigns[:recent_tweets] ).
            not_to eq( nil )
      end
    end # context 'with a signed in user'
  end # describe 'GET show'
end # describe UsersController
