require 'rails_helper'

describe FavoriteHelpers do
  controller(ActionController::Base) do
    include FavoriteHelpers
  end

  describe '#current_user_favorited_tweet?' do
    let(:user) { create_user }
    let(:tweet) { create_tweet }
    let(:subject) { controller.current_user_favorited_tweet?( tweet ) }

    before do
      sign_in user
    end

    context 'user has favorited the tweet' do
      before do
        create_favorite(
          user: user,
          tweet: tweet
        )
      end

      it 'should be true' do
        expect( subject ).
            to eq( true )
      end
    end # context 'user has favorited the tweet'

    context 'user has not favorited the tweet' do
      it 'should be false' do
        expect( subject ).
            to eq( false )
      end
    end # context 'user has not favorited the tweet'
  end
end
