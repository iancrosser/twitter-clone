require 'rails_helper'

describe Authorized do
  controller(ActionController::Base) do
    include Authorized

    def test
      render plain: ''
    end
  end

  before do
    routes.draw { get 'test' => 'anonymous#test' }
  end

  it_should_behave_like 'an authorized method', { via: :get, method: :test }
end
