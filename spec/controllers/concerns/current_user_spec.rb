require 'rails_helper'

describe CurrentUser do
  controller(ActionController::Base) do
    include CurrentUser
    def test
      render plain: ''
    end
  end

  include_examples 'from current user concern'
end
