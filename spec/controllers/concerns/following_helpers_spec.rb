require 'rails_helper'

describe FollowingHelpers do
  controller(ActionController::Base) do
    include FollowingHelpers
  end

  describe '#current_user_following_user?' do
    let(:user) { create_user }
    let(:another_user) { create_user }
    let(:subject) { controller.current_user_following_user?( another_user ) }

    before do
      sign_in user
    end

    context 'user is following the other user' do
      before do
        create_following(
          follower: user,
          followee: another_user
        )
      end

      it 'should be true' do
        expect( subject ).
            to eq( true )
      end
    end # context 'user is following the other user'

    context 'user is not following the other user' do
      before do
        create_following(
          follower: another_user,
          followee: user
        )
      end

      it 'should be false' do
        expect( subject ).
            to eq( false )
      end
    end # context 'user is not following the other user'
  end
end
