require 'rails_helper'

describe 'profiles routing' do
  context 'GET /profile' do
    it 'should route correctly' do
      expect( get: '/profile' ).
          to route_to(
            controller: 'profiles',
            action: 'show'
          )
    end
  end # context 'GET /profile'
end # describe 'profiles routing'
