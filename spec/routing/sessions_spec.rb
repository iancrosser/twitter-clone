require 'rails_helper'

describe 'sessions routing' do
  context 'POST /sessions' do
    it 'should route correctly' do
      expect( post: '/sessions' ).
          to route_to(
            controller: 'sessions',
            action: 'create'
          )
    end
  end # context 'POST /sessions'

  context 'GET /sign-out' do
    it 'should route correctly' do
      expect( get: '/sign-out' ).
          to route_to(
            controller: 'sessions',
            action: 'destroy'
          )
    end
  end # context 'GET /sign-out'
end # describe 'sessions routing'
