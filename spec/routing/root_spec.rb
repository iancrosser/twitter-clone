require 'rails_helper'

describe 'root routing' do
  context 'GET /' do
    it 'should route correctly' do
      expect( get: '/' ).
          to route_to(
            controller: 'users',
            action: 'new'
          )
    end
  end # context 'GET /'
end # describe 'users routing'
