require 'rails_helper'

describe 'favorites routing' do
  context 'GET /favorites' do
    it 'should route correctly' do
      expect( get: '/favorites' ).
          to route_to(
            controller: 'favorites',
            action: 'show'
          )
    end
  end # context 'GET /favorites'
end # describe 'favorites routing'
