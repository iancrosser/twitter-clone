require 'rails_helper'

describe 'timeline routing' do
  context 'GET /timeline' do
    it 'should route correctly' do
      expect( get: '/timeline' ).
          to route_to(
            controller: 'timelines',
            action: 'show'
          )
    end
  end # context 'GET /timeline'
end # describe 'timeline routing'
