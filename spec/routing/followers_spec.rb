require 'rails_helper'

describe 'followers routing' do
  context 'GET /followers' do
    it 'should route correctly' do
      expect( get: '/followers' ).
          to route_to(
            controller: 'followers',
            action: 'show'
          )
    end
  end # context 'GET /followers'
end # describe 'followers routing'
