require 'rails_helper'

describe 'tweets routing' do
  context 'POST /tweets' do
    it 'should route correctly' do
      expect( post: '/tweets' ).
          to route_to(
            controller: 'tweets',
            action: 'create'
          )
    end
  end # context 'POST /tweets'

  context 'POST /tweets/:tweet_id/favorite' do
    it 'should route correctly' do
      tweet = create_tweet
      expect( post: "/tweets/#{tweet.id}/favorite" ).
          to route_to(
            controller: 'tweets/favorites',
            action: 'create',
            tweet_id: tweet.id.to_s
          )
    end
  end # 'POST /tweets/:tweet_id/favorite'

  context 'DELETE /tweets/:tweet_id/favorite' do
    it 'should route correctly' do
      tweet = create_tweet
      expect( delete: "/tweets/#{tweet.id}/favorite" ).
          to route_to(
            controller: 'tweets/favorites',
            action: 'destroy',
            tweet_id: tweet.id.to_s
          )
    end
  end # 'DELETE /tweets/:tweet_id/favorite'
end # describe 'tweets routing'
