require 'rails_helper'

describe 'followings routing' do
  context 'GET /followings' do
    it 'should route correctly' do
      expect( get: '/followings' ).
          to route_to(
            controller: 'followings',
            action: 'show'
          )
    end
  end # context 'GET /followings'
end # describe 'followings routing'
