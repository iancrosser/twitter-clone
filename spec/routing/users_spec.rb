require 'rails_helper'

describe 'users routing' do
  context 'POST /users' do
    it 'should route correctly' do
      expect( post: '/users' ).
          to route_to(
            controller: 'users',
            action: 'create'
          )
    end
  end # context 'POST /users'

  context 'GET /users/:username' do
    it 'should route correctly' do
      username = user_default_attributes[:username]
      expect( get: "/users/#{username}" ).
          to route_to(
            controller: 'users',
            action: 'show',
            id: username
          )
    end
  end # context 'GET /users/:username'

  context 'POST /users/:username/followings' do
    it 'should route correctly' do
      username = user_default_attributes[:username]
      expect( post: "/users/#{username}/followings" ).
          to route_to(
            controller: 'users/followings',
            action: 'create',
            user_id: username
          )
    end
  end # 'POST /users/:username/followings'

  context 'DELETE /users/:username/followings' do
    it 'should route correctly' do
      username = user_default_attributes[:username]
      expect( delete: "/users/#{username}/followings" ).
          to route_to(
            controller: 'users/followings',
            action: 'destroy',
            user_id: username
          )
    end
  end # 'DELETE /users/:username/followings'
end # describe 'users routing'
