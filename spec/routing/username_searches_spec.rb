require 'rails_helper'

describe 'username_searches routing' do
  context 'POST /username_searches' do
    it 'should route correctly' do
      expect( post: '/username_searches' ).
          to route_to(
            controller: 'username_searches',
            action: 'create'
          )
    end
  end # context 'POST /username_searches'
end # describe 'username_searches routing'
