RSpec.shared_examples 'from current user concern' do
  describe '#current_user' do
    let(:subject) { controller.current_user }

    it 'should look in the session for the user' do
      user = create_user
      sign_in user

      expect( subject ).
          to eq( user )
    end

    it 'should return nil if not found' do
      sign_out

      expect( subject ).
          to eq( nil )
    end
  end # describe #current_user'

  describe '#signed_in?' do
    let(:subject) { controller.signed_in? }

    it 'should be true if current_user is present' do
      user = create_user
      sign_in user

      expect( subject ).
          to eq( true )
    end

    it 'should be false if current_user is not present' do
      sign_out

      expect( subject ).
          to eq( false )
    end
  end # describe '#signed_in?'
end
