RSpec.shared_examples 'an authorized method' do |options|
  describe '#require_user' do
    let(:subject) { send(options[:via], options[:method], params: options[:params]) }

    context 'with a signed in user' do
      before do
        allow( controller ).
            to receive( :current_user ).
            and_return create_user
      end

      it 'should not redirect' do
        expect( subject ).
            not_to redirect_to( root_path )
      end
    end # context 'with a signed in user'

    context 'without a signed in user' do
      it 'should redirect to the root url' do
        expect( subject ).
            to redirect_to( root_path )
      end
    end # context 'without a signed in user'
  end # describe '#require_user'
end
