require 'rails_helper'

describe ApplicationHelper do
  describe '#mention_link_content' do
    let(:subject) { helper.mention_link_content(tweet) }

    context 'without mentioned users' do
      let(:tweet) { create_tweet(content: 'no mentions here') }

      it 'should return the tweet content' do
        expect( subject ).
            to eq( tweet.content )
      end
    end # context 'without mentioned users'

    context 'with mentioned users' do
      let(:mentioned_user1) { create_user }
      let(:mentioned_user2) { create_user }
      let(:tweet) { TweetService.create_tweet(author: create_user, content: "hey #{Mention.delimiter}#{mentioned_user1.username} do you like #{Mention.delimiter}#{mentioned_user2.username}?") }

      it 'should link to the mentioned users' do
        expect( subject ).
            to include( Rails.application.routes.url_helpers.user_path(mentioned_user1.username) ).
            and include( Rails.application.routes.url_helpers.user_path(mentioned_user2.username) )
      end
    end # context 'with mentioned users'
  end # describe '#mention_link_content'
end # describe ApplicationHelper
