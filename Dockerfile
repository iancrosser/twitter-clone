FROM ruby:2.4.1

# update system and get the good stuff
RUN apt-get update -qq && apt-get install -y build-essential

# for nokogiri
RUN apt-get install -y libxml2-dev libxslt1-dev wget unzip

# for capybara-webkit and xvfb
RUN apt-get install -y --fix-missing libqt4-webkit libqt4-dev xvfb libgconf-2-4
RUN apt-get install -y --fix-missing chromium='55.0.2883.75-1~deb8u1'
RUN wget -N http://chromedriver.storage.googleapis.com/2.27/chromedriver_linux64.zip -P ~/
RUN unzip ~/chromedriver_linux64.zip -d ~/
RUN chmod +x ~/chromedriver
RUN mv -f ~/chromedriver /usr/bin/chromedriver

# fix rmagick
RUN ln -s /usr/lib/x86_64-linux-gnu/ImageMagick-6.8.9/bin-Q16/Magick* /usr/local/bin/
RUN ln -s /usr/lib/x86_64-linux-gnu/libMagickCore-6.Q16.so /usr/lib/x86_64-linux-gnu/libMagickCore.so

# for a JS runtime
RUN curl -sL https://deb.nodesource.com/setup_7.x | bash -
RUN apt-get install -y --force-yes nodejs

# npm to latest version
# Fix bug https://github.com/npm/npm/issues/9863
RUN cd $(npm root -g)/npm \
  && npm install fs-extra \
  && sed -i -e s/graceful-fs/fs-extra/ -e s/fs\.rename/fs.move/ ./lib/utils/rename.js
RUN npm install -g npm@3.10.7

# cleanup after ourselves
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# install gems
RUN mkdir /app
WORKDIR /app
ADD Gemfile /app/Gemfile
ADD Gemfile.lock /app/Gemfile.lock
ADD bundle_install.sh /app/bundle_install.sh

ARG PRECOMPILE_ASSETS
RUN ./bundle_install.sh ${PRECOMPILE_ASSETS}

# add code
ADD . /app

# precompile assets if set to
RUN ./prepare-production.sh ${PRECOMPILE_ASSETS}

# initial command
CMD bundle exec rails s -p $PORT -b '0.0.0.0'
