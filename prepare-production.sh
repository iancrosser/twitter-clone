#!/bin/bash
# prepare-production.sh

# precompile assets
if [ ! "$1" = "" ] ; then
  bundle exec rake assets:precompile RAILS_ENV=production RACK_ENV=production
fi
