require 'faker'
require 'literate_randomizer'

100.times do
  User.create!(
    email: Faker::Internet.unique.email,
    username: Faker::Internet.user_name(
      Faker::Name.unique.name,
      %w(_)
    ),
    password: 'pass1word',
    password_confirmation: 'pass1word'
  )
end

((User.count)..(25 * User.count)).to_a.sample.times do
  Tweet.create(
    content: LiterateRandomizer.sentence,
    author: User.all.sample
  )
end

User.all.each do |user|
  5.times do
    Following.create(
      followee: user,
      follower: User.all.sample
    )
  end
end
