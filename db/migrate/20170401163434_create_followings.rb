class CreateFollowings < ActiveRecord::Migration[5.0]
  def change
    create_table :followings do |t|
      t.timestamps
      t.integer :follower_id, null: false
      t.integer :followee_id, null: false
    end
  end
end
