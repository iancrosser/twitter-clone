class CreateTweets < ActiveRecord::Migration[5.0]
  def change
    create_table :tweets do |t|
      t.timestamps
      t.text :content, null: false
      t.integer :author_id, null: false
    end
  end
end
