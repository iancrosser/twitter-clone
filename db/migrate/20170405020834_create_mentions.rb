class CreateMentions < ActiveRecord::Migration[5.0]
  def change
    create_table :mentions do |t|
      t.timestamps
      t.integer :tweet_id, null: false
      t.integer :user_id, null: false
      t.string :replace_string, null: false
    end
  end
end
