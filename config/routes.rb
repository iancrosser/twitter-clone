Rails.application.routes.draw do
  resources :sessions, only: [:create]
  match '/sign-out', to: 'sessions#destroy', via: :get, as: :sign_out

  resources :users, only: [:create, :show] do
    scope module: :users do
      resource :followings, only: [:create, :destroy]
    end # scope module: :users
  end # resources :users

  resource :username_searches, only: [:create]
  resource :profile, only: [:show, :edit, :update]
  resource :followings, only: [:show]
  resource :followers, only: [:show]
  resource :favorites, only: [:show]

  resource :timeline, only: [:show]
  resources :tweets, only: [:create] do
    scope module: :tweets do
      resource :favorite, only: [:create, :destroy]
    end # scope module: :tweets
  end # resources :tweets

  root 'users#new'
end
