unless ENV.fetch('SEARCHBOX_URL', nil).nil?
  Elasticsearch::Model.client = Elasticsearch::Client.new host: ENV['SEARCHBOX_URL']
end
