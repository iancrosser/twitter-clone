#!/bin/bash
# bundle-install.sh

if [ ! "$1" = "" ] ; then
  bundle install --jobs 10 --retry 3 --without development test
else
  bundle install --jobs 10 --retry 3
fi
