#!/bin/bash

app_name=$1
tag=registry.heroku.com/$app_name/web

if [ ! "$1" = "" ] ; then
  docker build --build-arg="PRECOMPILE_ASSETS='true'" -t $tag .
  docker login --email=_ --username=_ --password=$(heroku auth:token) registry.heroku.com
  docker push $tag
  heroku run rake db:migrate -a $app_name
  heroku ps:restart -a $app_name
  watch heroku ps -a $app_name
  heroku open -a $app_name
else
  echo "Please provide the app name"
fi
