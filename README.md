# Twitter Clone
## for [Gaslight](https://teamgaslight.com/)

This application is meant to be a programming exercise that replicates many of the features of Twitter.

## For Information about running the app
- [Checkout the Wiki](../../wiki/Home)

## Herokuapp
- [https://gaslight-twitter-clone.herokuapp.com](https://gaslight-twitter-clone.herokuapp.com)